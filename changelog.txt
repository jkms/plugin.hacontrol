v0.3.0
- Actually turn lights on or off

v0.2.0
- Better settings menu
- Enumerate Light States

v0.1.0
- First release.
- Possible to enumerate and activate scenes

v0.0.1
- Initial version
