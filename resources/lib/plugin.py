# -*- coding: utf-8 -*-

import routing
import logging
import xbmcaddon
import sys
from resources.lib import kodiutils
from resources.lib import kodilogging
from xbmcgui import ListItem, Dialog
from xbmcplugin import addDirectoryItem, endOfDirectory, getSetting
import json
import requests

api_token = getSetting(int(sys.argv[1]), "ha_api_key")
api_port = getSetting(int(sys.argv[1]), "ha_port")
api_host = getSetting(int(sys.argv[1]), "ha_host")
api_ssl = getSetting(int(sys.argv[1]), "ha_ssl")

if api_ssl:
    api_url_base = 'https://{0}:{1}/api/'.format(api_host, api_port)
else:
    api_url_base = 'http://{0}:{1}/api/'.format(api_host, api_port)
headers = {'Content-Type': 'application/json',
           'Authorization': 'Bearer {0}'.format(api_token)}

ADDON = xbmcaddon.Addon()
logger = logging.getLogger(ADDON.getAddonInfo('id'))
kodilogging.config()
plugin = routing.Plugin()


def api_get_scenes():
    scenes = []
    response = requests.get('{0}states'.format(api_url_base), headers=headers)
    hastates = json.loads(response.content.decode('utf-8'))
    for entity in hastates:
        if "scene" in entity['entity_id']:
            scenes.append(entity)
    return scenes

def api_get_lights():
    lights = []
    response = requests.get('{0}states'.format(api_url_base), headers=headers)
    hastates = json.loads(response.content.decode('utf-8'))
    for entity in hastates:
        if "light" in entity['entity_id']:
            if 'hidden' in entity['attributes']:
                if entity['attributes']['hidden'] == False:
                    lights.append(entity)
            else:
                lights.append(entity)
    return lights


def api_enable_scene(scene_id):
    payload = {"entity_id": scene_id}
    response = requests.post('{0}services/scene/turn_on'.format(api_url_base),
                             headers=headers,
                             data=json.dumps(payload))
    if response.status_code == 200:
        return True
    else:
        return False


@plugin.route('/')
def index():
    addDirectoryItem(plugin.handle, plugin.url_for(
        show_lights), ListItem('Lights Status'), True)
    addDirectoryItem(plugin.handle, plugin.url_for(
        show_scenes), ListItem('Scenes'), True)
    endOfDirectory(plugin.handle)


@plugin.route('/show_lights')
def show_lights():
    lights = api_get_lights()
    lights_on = []
    for i, dic in enumerate(lights):
        if dic['state'] == "on":
            lights_on.append(i)

    dialog = Dialog()
    ret = dialog.multiselect("Choose something", [entity['attributes']['friendly_name'] for entity in lights], preselect=lights_on)
    if ret: 
        activate_lights = set(ret) - set(lights_on)
        for turn_on_light in activate_lights:
            # Dialog().ok('Turn on light', lights[turn_on_light]['attributes']['friendly_name'])
    	    payload = {"entity_id": lights[turn_on_light]['entity_id']}
	    response = requests.post('{0}services/light/turn_on'.format(api_url_base),
				    headers=headers,
				    data=json.dumps(payload))
    
        deactivate_lights =  set(lights_on) - set(ret)
        for turn_off_light in deactivate_lights:
            # Dialog().ok('Turn off light', lights[turn_off_light]['attributes']['friendly_name'])
    	    payload = {"entity_id": lights[turn_off_light]['entity_id']}
	    response = requests.post('{0}services/light/turn_off'.format(api_url_base),
				    headers=headers,
				    data=json.dumps(payload))
 
@plugin.route('/show_scenes')
def show_scenes():
    scenes = api_get_scenes()
    for scene in scenes:
        addDirectoryItem(plugin.handle, plugin.url_for(
            enable_scene, scene['entity_id']), ListItem(scene['attributes']['friendly_name']), True)
    endOfDirectory(plugin.handle)


@plugin.route('/enable_scene/<scene_id>')
def enable_scene(scene_id):
    if api_enable_scene(scene_id):
        Dialog().ok('Success!', "scene %s successfully activated!" % (scene_id))
    else:
        Dialog().ok('Failure', "scene %s failed" % (scene_id))
 
def run():
    plugin.run()
